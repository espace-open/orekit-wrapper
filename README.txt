Orekit (ORbit Extrapolation KIT) is a free java library
providing basic space dynamics objects and services.

It is licensed by CS Systèmes d'Information under the
Apache License Version 2.0. A copy of this license is
provided in the LICENSE.txt file.

The python-wrapper project allows using orekit in a
python environment.

